
## Pre

configura l'indirizzo ip statico del tuo pc

```
192.168.1.2
```

find your alsa device

```
aplay -l
```

guarda l'output e cerca il device number tipo:

```
card 1: USB [Scarlett 2i2 USB], device 0: USB Audio [USB Audio]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
```

in questo caso prendi il numero che viene dopo `card` e quello che viene
dopo `device` e vai a modificare il file chacha.liq mettendo

```
source = input.alsa(id = 'in_alsa', device = 'hw:<card>,<device>')
```

## Startup

from shell

```shell
liquidsoap chacha.liq
```
